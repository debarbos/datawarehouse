# LDAP Synchronization

It's possible to synchronize DataWarehouse groups with external LDAP groups by
enabling the `FF_LDAP_GROUP_SYNC` feature flag.

Groups configured to be synced by the LDAP integration are completely
managed by it. This means that users manually added to these Django groups
will be removed when the cron task runs.

Changes to the LDAP group are reflected when the cron updater runs. Newly
added users to the LDAP group automatically get added to the Django group
and removed users from the LDAP group are automatically removed from the
Django group. This does not affect users added via `extra_users`.

## Configuration

For LDAP queries to work it's necessary to setup a few configuration options
using environment variables:

<!-- markdownlint-disable line-length -->
| Name                 | Required | Description                                              |
|----------------------|----------|----------------------------------------------------------|
| `FF_LDAP_GROUP_SYNC` | `True`   | Enable LDAP synchronization.                             |
| `LDAP_SERVER_URL`    | `True`   | Address of the LDAP server.                              |
| `LDAP_BASE_SEARCH`   | `True`   | Base DN for the search.                                  |
| `LDAP_MEMBERS_FIELD` | `False`  | Name of the users in the result. Default: `uniqueMember` |
<!-- markdownlint-restore -->

## Mapping groups

To define the link between an LDAP search and a DataWarehouse group it's
necessary to create `LDAPGroupLink` objects, which can be done by an
administrator through the [admin interface].

The `LDAPGroupLink` object contains 2 required values: `group` and
`filter_query` plus an extra `extra_users` to force users to be part of the
group if they're not in the LDAP groups, which is useful for assigning service
accounts which are not linked to LDAP users.

For example:

- group: `policy_read_internal`
- filter_query:
  `(&(objectClass=rhatRoverGroup)(cn=cki-datawarehouse-internal-read))`
- extra_users: [`sa_reporter`]

[admin interface]: https://datawarehouse.cki-project.org/admin/
