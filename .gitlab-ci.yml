---
include:
  - project: cki-project/cki-lib
    file: .gitlab/ci_templates/cki-common.yml
    ref: production
  - template: Security/SAST.gitlab-ci.yml
  - template: Dependency-Scanning.gitlab-ci.yml

variables:
  SAST_DISABLED: 'true'
  DS_DEFAULT_ANALYZERS: ''

.sast:
  stage: 🐛
  rules:
    - when: always

sast-semgrep:
  extends: [semgrep-sast, .sast]

sast-brakeman:
  extends: [brakeman-sast, .sast]

dependency-scan:
  extends: [gemnasium-python-dependency_scanning, .sast]
  variables:
    SECURE_LOG_LEVEL: info
  before_script:
    # Workaround for:
    # error: Could not find suitable distribution for Requirement.parse(..)
    - python3 -m pip install git+https://github.com/kernelci/kcidb-io.git@v3_pre1
    - python3 -m pip install git+https://gitlab.com/cki-project/cki-lib.git

python_tests:
  extends: .tox
  services:
    # docker.io/postgres:alpine
    - name: quay.io/cki/mirror_postgres:alpine
      alias: postgres
  variables:
    POSTGRES_DB: datawarehouse
    POSTGRES_USER: datawarehouse
    POSTGRES_PASSWORD: password
    POSTGRES_HOST_AUTH_METHOD: trust
    DB_HOST: postgres
    SECRET_KEY: secret-key
    GITLAB_URL: https://gitlab.url
    BEAKER_URL: https://beaker.url

.images:
  parallel:
    matrix:
      - IMAGE_NAME: datawarehouse
      - IMAGE_NAME: nginx
        CHANGES: configs/*

publish:
  extends: [.publish, .images]

tag:
  extends: [.tag, .images]

deploy-production:
  extends: [.deploy_production_image, .images]

deploy-staging:
  extends: .deploy_production_image
  variables:
    IMAGE_NAME: datawarehouse
    DEPLOY_TAG: staging
  environment:
    name: staging/$IMAGE_NAME
    deployment_tier: staging

lint:
  extends: .tox
  script:
    - markdownlint .

lint-curlylint:
  extends: .tox
  script:
    - python3 -m pip install curlylint
    - python3 -m curlylint datawarehouse/templates/
  # https://github.com/thibaudcolas/curlylint/issues/132
  allow_failure: true

shellcheck:
  extends: .shellcheck

docs_build:
  extends: .tox
  script:
    - export PATH=~/.local/bin:${PATH}
    - python3 -m pip install -r docs-requirements.txt
    - mkdocs build -d public
    # Test links
    - python3 -m http.server 8080 --directory public &
    - linkchecker http://localhost:8080
  artifacts:
    expose_as: 'docs'
    paths:
      - public

.deploy_pages:
  variables:
    IMAGE_NAME: documentation
  dependencies:
    - docs_build
  artifacts:
    paths:
      - public

pages:
  extends: [.deploy_production_image, .deploy_pages]
  environment:
    url: https://cki-project.gitlab.io/datawarehouse
  script:
    - echo "Deploying documentation into production environment"

deploy-doc-mr:
  extends: [.deploy_mr_image, .deploy_pages]
  variables:
    AUTOMATIC_DEPLOYMENT: 'true'
  script:
    - echo "Deploying documentation into review environment"
  environment:
    url: "${CI_JOB_URL}/artifacts/file/public/index.html"
    on_stop: stop-doc-mr

stop-doc-mr:
  extends: [deploy-doc-mr, .stop_mr]
  script:
    - echo "Stopping documentation review environment"
