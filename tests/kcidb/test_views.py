"""Test the views module."""
import datetime

from django.utils import timezone

from datawarehouse import models
from tests import utils


class TestKCIDBViewsAnonymous(utils.KCIDBTestCase):
    """Test KCIDB frontend views."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_authorization.yaml',
    ]
    anonymous = True
    groups = []

    def test_checkout_view_with_id(self):
        """Test checkouts view querying with id."""
        self._ensure_test_conditions('read')

        for checkout in models.KCIDBCheckout.objects.all():
            response_iid = self.client.get(f'/kcidb/checkouts/{checkout.iid}')
            response_id = self.client.get(f'/kcidb/checkouts/{checkout.id}')

            self.assertEqual(response_iid.status_code, response_id.status_code)
            if response_id.status_code == 200:
                self.assertEqual(
                    response_iid.context['checkout'],
                    response_id.context['checkout']
                )

    def test_checkout_view(self):
        """Test checkouts view."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            response = self.client.get(f'/kcidb/checkouts/{checkout.iid}')

            if checkout not in authorized_checkouts:
                expected_code = 302 if self.anonymous else 404
                self.assertEqual(expected_code, response.status_code)
                continue

            all_issues_affected = set(
                list(models.Issue.objects.filter(kcidbcheckout=checkout).values_list('id', flat=True)) +
                list(models.Issue.objects.filter(kcidbbuild__checkout=checkout).values_list('id', flat=True)) +
                list(models.Issue.objects.filter(kcidbtest__build__checkout=checkout).values_list('id', flat=True))
            )
            self.assertContextEqual(
                response.context,
                {
                    'grouped_issues': [
                        {
                            'issue': issue,
                            'checkouts': models.IssueOccurrence.objects.filter(
                                kcidb_checkout=checkout, issue=issue),
                            'builds': models.IssueOccurrence.objects.filter(
                                kcidb_build__checkout=checkout, issue=issue),
                            'tests': models.IssueOccurrence.objects.filter(
                                kcidb_test__build__checkout=checkout, issue=issue),
                            'is_regression': False,
                        } for issue in models.Issue.objects.filter(id__in=all_issues_affected)
                    ],
                    'issues': models.Issue.objects.filter(resolved_at=None).order_by('-id'),
                    'tests': models.KCIDBTest.objects.filter(build__checkout=checkout),
                    'tests_failed': (
                        models.KCIDBTest.objects.filter(build__checkout=checkout)
                        .exclude(status=models.ResultEnum.PASS)
                    ),
                    'builds': models.KCIDBBuild.objects.filter(checkout=checkout),
                    'builds_failed': models.KCIDBBuild.objects.filter(checkout=checkout).exclude(valid=True),
                    'checkout': checkout,
                    'checkouts_failed': [checkout] if not checkout.valid else [],
                },
            )

    def test_build_view_with_id(self):
        """Test build view querying with id."""
        self._ensure_test_conditions('read')

        for build in models.KCIDBBuild.objects.all():
            response_iid = self.client.get(f'/kcidb/builds/{build.iid}')
            response_id = self.client.get(f'/kcidb/builds/{build.id}')

            self.assertEqual(response_iid.status_code, response_id.status_code)
            if response_id.status_code == 200:
                self.assertEqual(
                    response_iid.context['build'],
                    response_id.context['build']
                )

    def test_build_view(self):
        """Test build view."""
        self._ensure_test_conditions('read')
        authorized_builds = models.KCIDBBuild.objects.filter(
            checkout__id__in=self.checkouts_authorized['read']
        )

        for build in models.KCIDBBuild.objects.all():
            response = self.client.get(f'/kcidb/builds/{build.iid}')

            if build not in authorized_builds:
                expected_code = 302 if self.anonymous else 404
                self.assertEqual(expected_code, response.status_code)
                continue

            all_issues_affected = set(
                list(models.Issue.objects.filter(kcidbbuild=build).values_list('id', flat=True)) +
                list(models.Issue.objects.filter(kcidbtest__build=build).values_list('id', flat=True))
            )
            self.assertContextEqual(
                response.context,
                {
                    'grouped_issues': [
                        {
                            'issue': issue,
                            'checkouts': [],
                            'builds': models.IssueOccurrence.objects.filter(kcidb_build=build, issue=issue),
                            'tests': models.IssueOccurrence.objects.filter(kcidb_test__build=build, issue=issue),
                            'is_regression': False,
                        } for issue in models.Issue.objects.filter(id__in=all_issues_affected)
                    ],
                    'issues': models.Issue.objects.filter(resolved_at=None).order_by('-id'),
                    'tests': models.KCIDBTest.objects.filter(build=build),
                    'tests_failed': (
                        models.KCIDBTest.objects.filter(build=build).exclude(status=models.ResultEnum.PASS)
                    ),
                    'build': build,
                    'builds_failed': [build] if not build.valid else [],
                },
            )

    def test_test_view_with_id(self):
        """Test test view querying with id."""
        self._ensure_test_conditions('read')

        for test in models.KCIDBTest.objects.all():
            response_iid = self.client.get(f'/kcidb/tests/{test.iid}')
            response_id = self.client.get(f'/kcidb/tests/{test.id}')

            self.assertEqual(response_iid.status_code, response_id.status_code)
            if response_id.status_code == 200:
                self.assertEqual(
                    response_iid.context['test'],
                    response_id.context['test']
                )

    def test_test_view(self):
        """Test Test view."""
        self._ensure_test_conditions('read')
        authorized_tests = models.KCIDBTest.objects.filter(
            build__checkout__id__in=self.checkouts_authorized['read']
        )

        for test in models.KCIDBTest.objects.all():
            response = self.client.get(f'/kcidb/tests/{test.iid}')

            if test not in authorized_tests:
                expected_code = 302 if self.anonymous else 404
                self.assertEqual(expected_code, response.status_code)
                continue

            all_issues_affected = set(
                list(models.Issue.objects.filter(kcidbtest=test).values_list('id', flat=True))
            )
            self.assertContextEqual(
                response.context,
                {
                    'grouped_issues': [
                        {
                            'issue': issue,
                            'checkouts': [],
                            'builds': [],
                            'tests': models.IssueOccurrence.objects.filter(kcidb_test=test, issue=issue),
                            'is_regression': False,
                        } for issue in models.Issue.objects.filter(id__in=all_issues_affected)
                    ],
                    'issues': models.Issue.objects.filter(resolved_at=None).order_by('-id'),
                    'test': test,
                    'tests_failed': [test] if not test.status or test.status.name != 'PASS' else [],
                },
            )

    def test_create_issues_checkout(self):
        """Test linking an issue to a checkout."""
        self._ensure_test_conditions('write')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['write']
        )

        issue = models.Issue.objects.first()

        for checkout in models.KCIDBCheckout.objects.all():
            checkout.issues.clear()

            authorized = checkout in authorized_checkouts and not self.anonymous
            response_code = 302 if authorized else 404

            self.assert_authenticated_post(
                response_code,
                'change_kcidbcheckout',
                '/kcidb/issues/occurrences',
                {
                    'issue_id': issue.id,
                    'checkout_iids': [
                        checkout.iid
                    ],
                },
                user=self.user
            )

            if not authorized:
                continue

            self.assertEqual(
                issue,
                checkout.issues.get()
            )

    def test_create_issues_build(self):
        """Test linking an issue to builds."""
        self._ensure_test_conditions('write')
        authorized_builds = models.KCIDBBuild.objects.filter(
            checkout__id__in=self.checkouts_authorized['write']
        )

        issue = models.Issue.objects.first()

        for build in models.KCIDBBuild.objects.all():
            build.issues.clear()

            authorized = build in authorized_builds and not self.anonymous
            response_code = 302 if authorized else 404

            self.assert_authenticated_post(
                response_code,
                'change_kcidbbuild',
                '/kcidb/issues/occurrences',
                {
                    'issue_id': issue.id,
                    'build_iids': [
                        build.iid
                    ],
                },
                user=self.user
            )

            if not authorized:
                continue

            self.assertEqual(
                issue,
                build.issues.get()
            )

    def test_create_issues_test(self):
        """Test linking an issue to some tests."""
        self._ensure_test_conditions('write')
        authorized_tests = models.KCIDBTest.objects.filter(
            build__checkout__id__in=self.checkouts_authorized['write']
        )

        issue = models.Issue.objects.first()

        for test in models.KCIDBTest.objects.all():
            test.issues.clear()

            authorized = test in authorized_tests and not self.anonymous
            response_code = 302 if authorized else 404

            self.assert_authenticated_post(
                response_code,
                'change_kcidbtest',
                '/kcidb/issues/occurrences',
                {
                    'issue_id': issue.id,
                    'test_iids': [
                        test.iid
                    ],
                },
                user=self.user
            )

            if not authorized:
                continue

            self.assertEqual(
                issue,
                test.issues.get()
            )

    def test_create_issues_test_result(self):
        """Test linking an issue to some tests and test results."""
        self._ensure_test_conditions('write')
        authorized_tests = models.KCIDBTest.objects.filter(
            build__checkout__id__in=self.checkouts_authorized['write']
        )

        issue = models.Issue.objects.first()

        for test in models.KCIDBTest.objects.all():
            test.issues.clear()

            authorized = test in authorized_tests and not self.anonymous
            response_code = 302 if authorized else 404

            self.assert_authenticated_post(
                response_code,
                'change_kcidbtest',
                '/kcidb/issues/occurrences',
                {
                    'issue_id': issue.id,
                    'test_iids': [
                        test.iid
                    ],
                    f'test_{test.iid}_result_iids': [
                        test.kcidbtestresult_set.first().iid,
                    ]

                },
                user=self.user
            )

            if not authorized:
                continue

            issue_occurrence = models.IssueOccurrence.objects.get(issue=issue, kcidb_test=test)
            self.assertEqual(issue_occurrence.kcidb_testresult, test.kcidbtestresult_set.first())

    def test_create_issues_test_result_multiple(self):
        """Test linking an issue to some tests and test results."""
        self._ensure_test_conditions('write')
        authorized_tests = models.KCIDBTest.objects.filter(
            build__checkout__id__in=self.checkouts_authorized['write']
        )
        issue = models.Issue.objects.first()
        models.IssueOccurrence.objects.all().delete()

        self.assert_authenticated_post(
            302,
            'change_kcidbtest',
            '/kcidb/issues/occurrences',
            {
                'issue_id': issue.id,
                'test_iids': [
                    test.iid for test in authorized_tests
                ],
                **{f'test_{test.iid}_result_iids': list(test.kcidbtestresult_set.values_list('iid', flat=True))
                   for test in authorized_tests}
            },
            user=self.user
        )

        if self.anonymous:
            self.assertEqual(models.IssueOccurrence.objects.count(), 0)
        else:
            # Check we have one IssueOccurrence per KCIDBTestResult, and no extra as KCIDBTest is ignored.
            self.assertEqual(
                models.IssueOccurrence.objects.count(),
                models.KCIDBTestResult.objects.filter(test__in=authorized_tests).count(),
            )

    def test_create_issues_all(self):
        """
        Test linking an issue to checkouts, builds and tests.

        At least doesn't have authorization to one of the checkouts, so it should fail.
        """
        self._ensure_test_conditions('write')
        issue = models.Issue.objects.first()
        checkouts = models.KCIDBCheckout.objects.all()
        builds = models.KCIDBBuild.objects.all()
        tests = models.KCIDBTest.objects.all()

        self.assert_authenticated_post(
            404,
            ['change_kcidbcheckout', 'change_kcidbbuild', 'change_kcidbtest'],
            '/kcidb/issues/occurrences',
            {
                'issue_id': issue.id,
                'checkout_iids': list(checkouts.values_list('iid', flat=True)),
                'build_iids': list(builds.values_list('iid', flat=True)),
                'test_iids': list(tests.values_list('iid', flat=True))
            },
            user=self.user
        )

    def test_create_issues_all_allowed(self):
        """
        Test linking an issue to checkouts, builds and tests.

        All the submitted objects are allowed to this user.
        """
        self._ensure_test_conditions('write')
        issue = models.Issue.objects.first()
        checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized['write'])
        builds = models.KCIDBBuild.objects.filter(checkout__id__in=self.checkouts_authorized['write'])
        tests = models.KCIDBTest.objects.filter(build__checkout__id__in=self.checkouts_authorized['write'])

        [checkout.issues.clear() for checkout in checkouts]
        [build.issues.clear() for build in builds]
        [test.issues.clear() for test in tests]

        self.assert_authenticated_post(
            302,
            ['change_kcidbcheckout', 'change_kcidbbuild', 'change_kcidbtest'],
            '/kcidb/issues/occurrences',
            {
                'issue_id': issue.id,
                'checkout_iids': list(checkouts.values_list('iid', flat=True)),
                'build_iids': list(builds.values_list('iid', flat=True)),
                'test_iids': list(tests.values_list('iid', flat=True))
            },
            user=self.user
        )

        if self.anonymous:
            return

        for obj in checkouts:
            self.assertEqual(issue, obj.issues.get())
        for obj in builds:
            self.assertEqual(issue, obj.issues.get())
        for obj in tests:
            self.assertEqual(issue, obj.issues.get())

    def test_delete_issues_all(self):
        """
        Test deleting an issue from checkouts, builds and tests.

        At least doesn't have authorization to one of the checkouts, so it should fail.
        """
        self._ensure_test_conditions('write')
        issue = models.Issue.objects.first()
        checkouts = models.KCIDBCheckout.objects.all()
        builds = models.KCIDBBuild.objects.all()
        tests = models.KCIDBTest.objects.all()

        self.assert_authenticated_post(
            404,
            ['change_kcidbcheckout', 'change_kcidbbuild', 'change_kcidbtest'],
            '/kcidb/issues/occurrences',
            {
                'action': 'remove',
                'issue_id': issue.id,
                'checkout_iids': list(checkouts.values_list('iid', flat=True)),
                'build_iids': list(builds.values_list('iid', flat=True)),
                'test_iids': list(tests.values_list('iid', flat=True))
            },
            user=self.user
        )

    def test_delete_issues_all_allowed(self):
        """
        Test deleting an issue from checkouts, builds and tests.

        All the submitted objects are allowed to this user.
        """
        self._ensure_test_conditions('write')
        issue = models.Issue.objects.first()
        checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized['write'])
        builds = models.KCIDBBuild.objects.filter(checkout__id__in=self.checkouts_authorized['write'])
        tests = models.KCIDBTest.objects.filter(build__checkout__id__in=self.checkouts_authorized['write'])

        [checkout.issues.add(issue) for checkout in checkouts]
        [build.issues.add(issue) for build in builds]
        [test.issues.add(issue) for test in tests]

        self.assert_authenticated_post(
            302,
            ['change_kcidbcheckout', 'change_kcidbbuild', 'change_kcidbtest'],
            '/kcidb/issues/occurrences',
            {
                'action': 'remove',
                'issue_id': issue.id,
                'checkout_iids': list(checkouts.values_list('iid', flat=True)),
                'build_iids': list(builds.values_list('iid', flat=True)),
                'test_iids': list(tests.values_list('iid', flat=True))
            },
            user=self.user
        )

        if self.anonymous:
            return

        for obj in checkouts:
            self.assertFalse(obj.issues.filter(id=issue.id).exists())
        for obj in builds:
            self.assertFalse(obj.issues.filter(id=issue.id).exists())
        for obj in tests:
            self.assertFalse(obj.issues.filter(id=issue.id).exists())


class TestKCIDBViewsNoGroup(TestKCIDBViewsAnonymous):
    """TestKCIDBViews with a group with no groups assigned."""

    anonymous = False
    groups = []


class TestKCIDBViewsReadGroups(TestKCIDBViewsAnonymous):
    """TestKCIDBViews with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestKCIDBViewsWriteGroups(TestKCIDBViewsAnonymous):
    """TestKCIDBViews with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestKCIDBViewsAllGroups(TestKCIDBViewsAnonymous):
    """TestKCIDBViews with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestCheckoutsListAnonymous(utils.KCIDBTestCase):
    """Test the checkouts list view."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
    ]
    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized[method])
        no_auth_checkouts = models.KCIDBCheckout.objects.exclude(id__in=self.checkouts_authorized[method])

        checks = [
            (auth_checkouts, 'No authorized checkouts'),
            (no_auth_checkouts, 'No unauthorized checkouts'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def setUp(self):
        """Set up data."""
        origin = models.KCIDBOrigin.objects.get(name='redhat')
        public_policy = models.Policy.objects.get(name='public')
        restricted_policy = models.Policy.objects.get(name='restricted')

        models.KCIDBCheckout.objects.bulk_create(
            [
                models.KCIDBCheckout(
                    origin=origin,
                    id=f'public_{index}',
                    policy=public_policy,
                    git_repository_branch='public_branch',
                )
                for index in range(40)
            ]
        )
        models.KCIDBCheckout.objects.bulk_create(
            [
                models.KCIDBCheckout(
                    origin=origin,
                    id=f'restricted_{index}',
                    policy=restricted_policy,
                    git_repository_branch='restricted_branch',
                )
                for index in range(40)
            ]
        )
        models.KCIDBCheckout.objects.bulk_create(
            [
                models.KCIDBCheckout(
                    origin=origin,
                    id=f'unavailable_{index}',
                    policy=None,
                    git_repository_branch='unavailable_branch',
                )
                for index in range(40)
            ]
        )

        super().setUp()

    def test_checkout_list(self):
        """Test checkouts list."""
        checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized['read'])
        response = self.client.get('/kcidb/checkouts')
        self.assertContextEqual(
            response.context,
            {
                'checkouts': checkouts[:30],
                'git_branches': (
                    checkouts
                    .order_by('git_repository_branch')
                    .distinct('git_repository_branch')
                    .values_list('git_repository_branch', flat=True)
                ),
            },
        )

    def test_checkout_list_page_2(self):
        """Test checkouts list pagination."""
        checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized['read'])
        response = self.client.get('/kcidb/checkouts?page=2')
        self.assertContextEqual(
            response.context,
            {
                'checkouts': checkouts[30:60],
            },
        )

    def test_checkout_list_page_invalid(self):
        """Test checkouts list pagination with wrong page numbers."""
        checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized['read'])
        for page in ('...', 'foobar', '3%3BSELECT%20sleep%2829%29%3B%20--', '0', '-1', '-1000'):
            response = self.client.get(f'/kcidb/checkouts?page={page}')
            self.assertContextEqual(
                response.context,
                {
                    'checkouts': checkouts[:30],
                },
            )

    def test_checkout_list_aggregated(self):
        """Test checkouts list returns aggregated results."""
        response = self.client.get('/kcidb/checkouts')
        self.assertTrue(
            hasattr(
                response.context['checkouts'][0],
                'stats_tests_fail_count'
            )
        )

    def test_failures_filtered(self):
        """Test checkouts list filtered."""
        models.KCIDBCheckout.objects.all().delete()

        origin = models.KCIDBOrigin.objects.first()

        policy_permissive = models.Policy.objects.create(
            name='permissive', write_group=None, read_group=None
        )
        tree_1 = models.GitTree.objects.create(name='tree_1')
        tree_2 = models.GitTree.objects.create(name='tree_2')

        rev_1 = models.KCIDBCheckout.objects.create(id='1', origin=origin, tree=tree_1, policy=policy_permissive)
        rev_2 = models.KCIDBCheckout.objects.create(id='2', origin=origin, tree=tree_2, policy=policy_permissive)

        # Filter tree_1
        response = self.client.get('/kcidb/checkouts?filter_gittrees=tree_1')
        self.assertContextEqual(response.context, {'checkouts': [rev_1]})

        # Filter tree_2
        response = self.client.get('/kcidb/checkouts?filter_gittrees=tree_2')
        self.assertContextEqual(response.context, {'checkouts': [rev_2]})


class TestCheckoutsListNoGroups(TestCheckoutsListAnonymous):
    """TestCheckoutList with no groups."""

    anonymous = False
    groups = []


class TestCheckoutsListReadGroups(TestCheckoutsListAnonymous):
    """TestCheckoutList with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestCheckoutsListWriteGroups(TestCheckoutsListAnonymous):
    """TestCheckoutList with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestCheckoutsListAllGroups(TestCheckoutsListAnonymous):
    """TestCheckoutList with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestCheckoutsFailuresAnonymous(utils.KCIDBTestCase):
    """Test the checkouts failures list view."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_failed_checkouts.yaml'
    ]
    anonymous = True
    groups = []

    def test_failures_all(self):
        """Test failures list. All."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/all')
        self.assertContextEqual(
            response.context,
            {
                'checkouts': (
                    models.KCIDBCheckout.objects
                    .filter(id__in=self.checkouts_authorized['read'])
                    .exclude(iid__in=(5, 10, 15))  # These ones do not have failures
                )
            },
        )

    def test_failures_checkouts(self):
        """Test failures list. Checkouts."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/checkout')
        self.assertContextEqual(
            response.context,
            {
                'checkouts': (
                    models.KCIDBCheckout.objects
                    .filter(id__in=self.checkouts_authorized['read'])
                    .filter(valid=False)
                )
            },
        )

    def test_failures_builds(self):
        """Test failures list. Builds."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/build')
        self.assertContextEqual(
            response.context,
            {
                'checkouts': (
                    models.KCIDBCheckout.objects
                    .filter(id__in=self.checkouts_authorized['read'])
                    .filter(kcidbbuild__valid=False)
                )
            },
        )

    def test_failures_tests(self):
        """Test failures list. Tests."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/test')
        print(models.KCIDBTest.objects.filter(build__checkout__id='restricted_checkout_4'))
        self.assertContextEqual(
            response.context,
            {
                'checkouts': (
                    models.KCIDBCheckout.objects
                    .filter(id__in=self.checkouts_authorized['read'])
                    .filter(kcidbbuild__kcidbtest__status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES)
                )
            },
        )

    def test_pagination(self):
        """Test list pagination."""
        self._ensure_test_conditions('read')
        # Remove previous objects
        models.KCIDBCheckout.objects.all().delete()

        origin = models.KCIDBOrigin.objects.first()
        public_policy = models.Policy.objects.get(name='public')
        models.KCIDBCheckout.objects.bulk_create([
            models.KCIDBCheckout(
                id=f'public_checkout_{index}',
                origin=origin,
                valid=False,
                policy=public_policy,
            ) for index in range(40)
        ])

        for page in ('2', 'last'):
            response = self.client.get(f'/kcidb/failures/checkout?page={page}')

            self.assertContextEqual(
                response.context,
                {
                    'checkouts': models.KCIDBCheckout.objects.all()[30:],
                },
            )

    def test_list_aggregated(self):
        """Test checkouts list returns aggregated results."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/failures/all')
        self.assertTrue(
            hasattr(
                response.context['checkouts'][0],
                'stats_tests_fail_count'
            )
        )

    def test_unknown(self):
        """Test checkouts list returns aggregated results."""
        response = self.client.get('/kcidb/failures/foobar')
        self.assertEqual(400, response.status_code)
        self.assertEqual(b'Not sure what foobar is.', response.content)

    def test_failures_filtered(self):
        """Test failures list. Filtered."""
        checkout = models.KCIDBCheckout.objects.last()
        checkout.tree = models.GitTree.objects.create(name='tree_2')
        checkout.policy = models.Policy.objects.create(
            name='permissive', write_group=None, read_group=None
        )
        checkout.save()

        response = self.client.get('/kcidb/failures/all?filter_gittrees=tree_2')
        self.assertTrue(models.KCIDBCheckout.objects.filter(tree__name='tree_2').exists())
        self.assertContextEqual(
            response.context,
            {
                'checkouts': (
                    models.KCIDBCheckout.objects.filter(tree__name='tree_2')
                    .exclude(id='restricted_checkout_5')  # Successful
                )
            },
        )


class TestCheckoutsFailuresNoGroups(TestCheckoutsFailuresAnonymous):
    """TestCheckoutsFailures with no groups."""

    anonymous = False
    groups = []


class TestCheckoutsFailuresReadGroup(TestCheckoutsFailuresAnonymous):
    """TestCheckoutsFailures with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestCheckoutsFailuresWriteGroup(TestCheckoutsFailuresAnonymous):
    """TestCheckoutsFailures with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestCheckoutsFailuresAllGroups(TestCheckoutsFailuresAnonymous):
    """TestCheckoutsFailures with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestCheckoutsSearchAnonymous(utils.KCIDBTestCase):
    """Test the checkouts search view."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_authorization.yaml',
    ]

    anonymous = False
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_checkouts = models.KCIDBCheckout.objects.filter(id__in=self.checkouts_authorized[method])
        no_auth_checkouts = models.KCIDBCheckout.objects.exclude(id__in=self.checkouts_authorized[method])

        checks = [
            (auth_checkouts, 'No authorized checkouts'),
            (no_auth_checkouts, 'No unauthorized checkouts'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def test_search_empty(self):
        """Test search view with empty query."""
        self._ensure_test_conditions('read')
        response = self.client.get('/search')
        self.assertContextEqual(response.context, {})

        response = self.client.get('/search?q=')
        self.assertContextEqual(response.context, {})

    def test_search_not_found(self):
        """Test search view with empty query."""
        self._ensure_test_conditions('read')
        response = self.client.get('/search?q=foobar')
        self.assertContextEqual(response.context, {})

    def test_search_by_id(self):
        """Test search view with id."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            # add some spaces to test the stripping
            response = self.client.get(f'/search?q=+{checkout.id}+')
            self.assertEqual(200, response.status_code)

            if checkout not in authorized_checkouts:
                expected = {'checkouts': []}
            else:
                expected = {'checkouts': [checkout]}

            self.assertContextEqual(response.context, expected)

    def test_search_by_iid(self):
        """Test search view with iid."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            response = self.client.get(f'/search?q={checkout.iid}')
            self.assertEqual(200, response.status_code)

            if checkout not in authorized_checkouts:
                expected = {'checkouts': []}
            else:
                expected = {'checkouts': [checkout]}

            self.assertContextEqual(response.context, expected)

    def test_search_by_kernel_version(self):
        """Test search view with kernel version."""
        self._ensure_test_conditions('read')
        authorized_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized['read']
        )

        for checkout in models.KCIDBCheckout.objects.all():
            response = self.client.get(f'/search?q={checkout.kernel_version}')
            self.assertEqual(200, response.status_code)

            if checkout not in authorized_checkouts:
                expected = {'checkouts': []}
            else:
                expected = {'checkouts': [checkout]}

            self.assertContextEqual(response.context, expected)


class TestCheckoutsSearchReadGroup(TestCheckoutsSearchAnonymous):
    """Test the checkouts search view. Read group."""

    anonymous = False
    groups = ['group_a']


class TestCheckoutsSearchWriteGroup(TestCheckoutsSearchAnonymous):
    """Test the checkouts search view. Write group."""

    anonymous = False
    groups = ['group_b']


class TestCheckoutsSearchAllGroups(TestCheckoutsSearchAnonymous):
    """Test the checkouts search view. All groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestBuildsListAnonymous(utils.KCIDBTestCase):
    """Test the builds list view."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_multiple_policies.yaml',
        'tests/kcidb/fixtures/multiple_build_package_names.yaml',
    ]
    anonymous = True
    groups = []

    def test_builds_list(self):
        """Test builds list."""
        builds = models.KCIDBBuild.objects.filter(checkout__id__in=self.checkouts_authorized['read'])
        response = self.client.get('/kcidb/builds')
        self.assertContextEqual(
            response.context,
            {
                'builds': builds,
                'architectures': models.ArchitectureEnum,
                'package_names': ['kernel', 'kernel-automotive', 'kernel-rt', None],
            },
        )

    def test_builds_list_aggregated(self):
        """Test builds list returns aggregated results."""
        response = self.client.get('/kcidb/builds')
        self.assertTrue(
            hasattr(
                response.context['builds'][0],
                'stats_tests_fail_count'
            )
        )

    def test_builds_list_filter(self):
        """Test builds list filtered."""
        builds = models.KCIDBBuild.objects.filter(checkout__id__in=self.checkouts_authorized['read'])
        response = self.client.get('/kcidb/builds?filter_architectures=2')
        self.assertContextEqual(response.context, {'builds': builds.filter(architecture=2)})

        response = self.client.get('/kcidb/builds?filter_architectures=3')
        self.assertContextEqual(response.context, {'builds': builds.filter(architecture=3)})

        response = self.client.get('/kcidb/builds?filter_architectures=2&filter_architectures=3')
        self.assertContextEqual(response.context, {'builds': builds.filter(architecture__in=[2, 3])})

        response = self.client.get('/kcidb/builds?filter_package_names=kernel&filter_package_names=kernel-rt')
        self.assertContextEqual(response.context, {'builds': builds.filter(package_name__in=['kernel', 'kernel-rt'])})

    def test_builds_list_checkout_filter(self):
        """Test builds list filtered by checkout params."""
        builds = models.KCIDBBuild.objects.filter(checkout__id__in=self.checkouts_authorized['read'])
        response = self.client.get('/kcidb/builds?filter_gittrees=tree_1')
        self.assertContextEqual(response.context, {'builds': builds.filter(checkout__tree__name='tree_1')})

        response = self.client.get('/kcidb/builds?filter_gittrees=tree_2')
        self.assertContextEqual(response.context, {'builds': builds.filter(checkout__tree__name='tree_2')})

        response = self.client.get('/kcidb/builds?filter_gittrees=tree_1&filter_gittrees=tree_2')
        self.assertContextEqual(
            response.context, {'builds': builds.filter(checkout__tree__name__in=['tree_1', 'tree_2'])}
        )


class TestBuildsListNoGroups(TestBuildsListAnonymous):
    """TestBuildList with no groups."""

    anonymous = False
    groups = []


class TestBuildsListReadGroups(TestBuildsListAnonymous):
    """TestBuildList with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestBuildsListWriteGroups(TestBuildsListAnonymous):
    """TestBuildList with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestBuildsListAllGroups(TestBuildsListAnonymous):
    """TestBuildList with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestBaselinesListAnonymous(utils.KCIDBTestCase):
    """Test the checkouts list view."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/fixtures/base_simple.yaml',
        'tests/kcidb/fixtures/basic.yaml',
    ]
    anonymous = True
    groups = []

    def _ensure_test_conditions(self, method):
        """Check that all conditions for testing are optimal."""
        auth_checkouts = models.KCIDBCheckout.objects.filter(
            id__in=self.checkouts_authorized[method],
        ).exclude(
            related_merge_request__isnull=False
        )
        no_auth_checkouts = models.KCIDBCheckout.objects.exclude(
            id__in=self.checkouts_authorized[method],
            related_merge_request__isnull=False
        )

        checks = [
            (auth_checkouts, 'No authorized checkouts'),
            (no_auth_checkouts, 'No unauthorized checkouts'),
        ]

        for check, message in checks:
            self.assertTrue(check.exists(), f'Test conditions unmet: {message} for "{method}"')

    def setUp(self):
        """Set up data."""
        origin = models.KCIDBOrigin.objects.get(name='redhat')
        public_policy = models.Policy.objects.get(name='public')
        restricted_policy = models.Policy.objects.get(name='restricted')

        models.KCIDBCheckout.objects.bulk_create(
            [
                models.KCIDBCheckout(
                    origin=origin,
                    id=f'public_{index}',
                    policy=public_policy,
                    git_repository_url='tree_1',
                    git_repository_branch='branch_1',
                    tree=models.GitTree.objects.get_or_create(name='tree_1')[0],
                    start_time=timezone.now(),
                    scratch=bool(index % 2),
                )
                for index in range(10)
            ]
        )
        models.KCIDBCheckout.objects.bulk_create(
            [
                models.KCIDBCheckout(
                    origin=origin,
                    id=f'restricted_{index}',
                    policy=restricted_policy,
                    git_repository_url='tree_2',
                    git_repository_branch='branch_2',
                    tree=models.GitTree.objects.get_or_create(name='tree_2')[0],
                    start_time=timezone.now(),
                    scratch=bool(index % 2),
                )
                for index in range(10)
            ]
        )
        models.KCIDBCheckout.objects.bulk_create(
            [
                models.KCIDBCheckout(
                    origin=origin,
                    id=f'unavailable_{index}',
                    policy=None,
                    git_repository_url='tree_3',
                    git_repository_branch='branch_3',
                    tree=models.GitTree.objects.get_or_create(name='tree_3')[0],
                    start_time=timezone.now(),
                    scratch=bool(index % 2),
                )
                for index in range(10)
            ]
        )

        super().setUp()

    def test_baselines_detection(self):
        """Ensure we're only showing baselines."""
        self._ensure_test_conditions('read')

        models.KCIDBCheckout.objects.update(scratch=True)

        response = self.client.get('/kcidb/baselines')
        self.assertContextEqual(response.context, {'checkouts': []})

    def test_baselines_list(self):
        """Test list baselines."""
        self._ensure_test_conditions('read')
        checkouts = (
            models.KCIDBCheckout.objects
            .filter(
                id__in=self.checkouts_authorized['read'],
                scratch=False,
            )
            .order_by('git_repository_url', 'git_repository_branch', '-iid')
            .distinct('git_repository_url', 'git_repository_branch')
        )
        response = self.client.get('/kcidb/baselines')
        self.assertContextEqual(
            response.context,
            {
                'checkouts': checkouts,
                'architectures': models.ArchitectureEnum,
            },
        )

    def test_baselines_time_filter(self):
        """Test list baselines only shows checkouts newer than 60 days."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/baselines')
        self.assertTrue(len(response.context['checkouts']))

        # Less than 60 days, should be included
        models.KCIDBCheckout.objects.update(start_time=timezone.now() - datetime.timedelta(days=59))
        response = self.client.get('/kcidb/baselines')
        self.assertTrue(len(response.context['checkouts']))

        # More than 60 days, should not be included
        models.KCIDBCheckout.objects.update(start_time=timezone.now() - datetime.timedelta(days=61))
        response = self.client.get('/kcidb/baselines')
        self.assertFalse(len(response.context['checkouts']))

    def test_baselines_get(self):
        """Test getting baselines for a given tree."""
        self._ensure_test_conditions('read')
        checkouts = (
            models.KCIDBCheckout.objects
            .filter(id__in=self.checkouts_authorized['read'])
            .order_by('tree', '-iid')
        )
        for index in range(1, 4):
            response = self.client.get('/kcidb/baselines/history?'
                                       f'git_repository_url=tree_{index}'
                                       f'&git_repository_branch=branch_{index}'
                                       f'&tree_name=tree_{index}')
            self.assertContextEqual(
                response.context,
                {
                    'checkouts': checkouts.filter(
                        git_repository_url=f'tree_{index}',
                        git_repository_branch=f'branch_{index}',
                        tree__name=f'tree_{index}'
                    ),
                    'architectures': models.ArchitectureEnum,
                },
            )

    def test_baselines_get_redirect(self):
        """Test get baseline without tree redirects to the main baselines page."""
        self._ensure_test_conditions('read')
        response = self.client.get('/kcidb/baselines/history')
        self.assertRedirects(response, '/kcidb/baselines')


class TestBaselinesListReadGroups(TestBaselinesListAnonymous):
    """TestBuildList with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestBaselinesListWriteGroups(TestBaselinesListAnonymous):
    """TestBuildList with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestBaselinesListAllGroups(TestBaselinesListAnonymous):
    """TestBuildList with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']


class TestTestsListAnonymous(utils.KCIDBTestCase):
    """Test the tests list view."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_authorization.yaml',
        'tests/kcidb/fixtures/base_time.yaml',
    ]
    anonymous = True
    groups = []

    def test_tests_list(self):
        """Test tests list."""
        self._ensure_test_conditions('read')
        tests = (
            models.KCIDBTest.objects
            .filter(build__checkout__id__in=self.checkouts_authorized['read'])
            .order_by('-iid')
        )
        response = self.client.get('/kcidb/tests')
        self.assertContextEqual(
            response.context,
            {
                'tests': tests,
            },
        )

    def test_tests_list_filter(self):
        """Test tests list filtered by checkout params."""
        self._ensure_test_conditions('read')
        tests = (
            models.KCIDBTest.objects
            .filter(build__checkout__id__in=self.checkouts_authorized['read'])
            .order_by('-iid')
        )
        response = self.client.get('/kcidb/tests?tree_filter=tree_[1]')
        self.assertContextEqual(response.context, {'tests': tests.filter(build__checkout__tree__name='tree_1')})

        response = self.client.get('/kcidb/tests?arch_filter=aarch64')
        self.assertContextEqual(response.context, {'tests': tests.filter(build__architecture=2)})

        response = self.client.get(f'/kcidb/tests?test_filter={tests.first().test.name}.*')
        self.assertContextEqual(response.context, {'tests': tests.filter(test=tests.first().test)})

        response = self.client.get(f'/kcidb/tests?host_filter={tests.first().environment.fqdn}.*')
        self.assertContextEqual(response.context, {'tests': tests.filter(environment=tests.first().environment)})

        response = self.client.get('/kcidb/tests?result_filter=FAIL')
        self.assertContextEqual(response.context, {'tests': tests.filter(status='FAIL')})

        response = self.client.get('/kcidb/tests?issues_tagged_filter=True')
        self.assertContextEqual(response.context, {'tests': tests.exclude(issues=None)})

        kernel_version = tests.first().build.checkout.kernel_version
        response = self.client.get(f'/kcidb/tests?kernel_version_filter={kernel_version}')
        self.assertContextEqual(response.context, {
            'tests': tests.filter(build__checkout__kernel_version=kernel_version)
        })

        response = self.client.get('/kcidb/tests?sort_by_start_time=True')
        self.assertContextEqual(response.context, {
            'tests': tests.filter(start_time__isnull=False).order_by('-start_time')
        })


class TestTestsListNoGroups(TestTestsListAnonymous):
    """TestTestList with no groups."""

    anonymous = False
    groups = []


class TestTestsListReadGroups(TestTestsListAnonymous):
    """TestTestList with a group with read authorization."""

    anonymous = False
    groups = ['group_a']


class TestTestsListWriteGroups(TestTestsListAnonymous):
    """TestTestList with a group with write authorization."""

    anonymous = False
    groups = ['group_b']


class TestTestsListAllGroups(TestTestsListAnonymous):
    """TestTestList with all groups."""

    anonymous = False
    groups = ['group_a', 'group_b']
