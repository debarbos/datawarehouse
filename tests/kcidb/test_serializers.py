"""Test kcidb serializers."""
import kcidb_io

from datawarehouse import models
from datawarehouse.api.kcidb import serializers
from tests import utils


class TestSerializers(utils.TestCase):
    # pylint: disable=too-many-instance-attributes
    """Test KCIDB-schemed serializers."""

    schema = kcidb_io.schema.v4.VERSION
    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_complete.yaml'
    ]

    def test_checkout(self):
        """Test KCIDBCheckout object seralization is valid."""
        data = {
            'version': {
                'major': self.schema.major,
                'minor': self.schema.minor,
            },
            'checkouts': [
                serializers.KCIDBCheckoutSerializer(
                    models.KCIDBCheckout.objects.get(iid=1)
                ).data
            ]
        }

        self.schema.validate_exactly(data)

    def test_build(self):
        """Test KCIDBBuild object seralization is valid."""
        data = {
            'version': {
                'major': self.schema.major,
                'minor': self.schema.minor,
            },
            'builds': [
                serializers.KCIDBBuildSerializer(
                    models.KCIDBBuild.objects.get(iid=1)
                ).data
            ]
        }

        self.schema.validate_exactly(data)

    def test_build_misc_revision_id(self):
        """Test compatibility revision_id."""
        build = serializers.KCIDBBuildSerializer(
            models.KCIDBBuild.objects.get(iid=1)
        ).data
        self.assertEqual(
            ('403cbf29a4e277ad4872515ec3854b175960bbdf'
             '+a18c010ffcf8852321ceee0820c766974567e3eabf348afefccfa2fec2b64e2b'),
            build['misc']['revision_id']
        )

    def test_test(self):
        """Test KCIDBTest object seralization is valid."""
        data = {
            'version': {
                'major': self.schema.major,
                'minor': self.schema.minor,
            },
            'tests': [
                serializers.KCIDBTestSerializer(
                    models.KCIDBTest.objects.get(iid=1)
                ).data
            ]
        }

        self.schema.validate_exactly(data)
