"""Tests for recipient parsing."""
from unittest import mock

from django import test

from datawarehouse import models
from datawarehouse import recipients
from datawarehouse.utils import datetime_bool
from tests import utils


class TestRecipientsMethods(test.TestCase):
    """Tests for recipient parsing."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/checkouts_and_recipients.yaml',
    ]

    def test_when_always(self):
        """Test the when_always function."""
        self.assertTrue(recipients.when_always(None))

    def test_when_failed(self):
        """Test the when_failed function."""
        checkout = models.KCIDBCheckout.objects.get()
        build = checkout.kcidbbuild_set.get()
        test = build.kcidbtest_set.first()
        cases = [
            # obj_to_patch, patched_attrs, expected result
            (checkout,  {'valid': True},    False),
            (checkout,  {'valid': False},   True),
            (build,     {'valid': False},   True),
            (test,      {'status': 'E'},    False),
            (test,      {'status': 'S'},    False),
            (test,      {'status': None},   False),
            (test,      {'status': 'F',
                         'waived': True},   False),
            (test,      {'status': 'F'},    True),
        ]

        for obj, patched_attrs, expected in cases:
            with utils.patched_object(obj, patched_attrs):
                checkout = models.KCIDBCheckout.objects.aggregated().get()  # Refresh aggregated()
                self.assertEqual(recipients.when_failed(checkout), expected, patched_attrs)

    def test_when_failed_tests(self):
        """Test the when_failed_tests function."""
        checkout = models.KCIDBCheckout.objects.get()
        build = checkout.kcidbbuild_set.get()
        test = build.kcidbtest_set.first()
        cases = [
            # obj_to_patch, patched_attrs, expected result
            (checkout,  {'valid': True},    False),
            (checkout,  {'valid': False},   False),
            (build,     {'valid': False},   False),
            (test,      {'status': 'E'},    False),
            (test,      {'status': 'S'},    False),
            (test,      {'status': None},   False),
            (test,      {'status': 'F',
                         'waived': True},   False),
            (test,      {'status': 'F'},    True),
        ]

        for obj, patched_attrs, expected in cases:
            with utils.patched_object(obj, patched_attrs):
                checkout = models.KCIDBCheckout.objects.aggregated().get()  # Refresh aggregated()
                self.assertEqual(recipients.when_failed_tests(checkout), expected)

    def test_failed_test_maintainers(self):
        """Test failed_tests_maintainers function."""
        checkout = models.KCIDBCheckout.objects.get()
        test = checkout.kcidbbuild_set.get().kcidbtest_set.first()
        cases = [
            # obj_to_patch, patched_attrs, expected result
            (test,  {'status': 'P', 'waived': False},   []),
            (test,  {'status': 'S', 'waived': False},   []),
            (test,  {'status': None, 'waived': False},  []),
            (test,  {'status': 'F', 'waived': False},   ['maint1@email.com']),
            (test,  {'status': 'E', 'waived': False},   []),
            (test,  {'status': 'P', 'waived': True},    []),
            (test,  {'status': 'F', 'waived': True},    []),
            (test,  {'status': 'E', 'waived': True},    []),
        ]

        for obj, patched_attrs, expected in cases:
            with utils.patched_object(obj, patched_attrs):
                self.assertEqual(
                    recipients.failed_tests_maintainers(checkout),
                    expected,
                    (patched_attrs, expected)
                )

    def test_failed_test_maintainers_issues(self):
        """Test failed_tests_maintainers function when tests have issues tagged."""
        checkout = models.KCIDBCheckout.objects.get()
        test = checkout.kcidbbuild_set.get().kcidbtest_set.first()
        test.status = 'F'
        test.save()

        cases = [
            # issues, expected result
            # No issues, return maintainer
            ([],                                        ['maint1@email.com']),
            # Has not-resolved issue tagged, don't return maintainer.
            ([{'resolved': False}],                     []),
            # Has at least one resolved isuse, return maintainer.
            ([{'resolved': True}],                      ['maint1@email.com']),
            ([{'resolved': False}, {'resolved': True}], ['maint1@email.com']),
        ]

        for issues, expected in cases:
            for i, issue in enumerate(issues):
                models.IssueOccurrence.objects.create(
                    kcidb_test=test,
                    issue=models.Issue.objects.create(
                        ticket_url=i,
                        resolved_at=datetime_bool(issue.get('resolved', False))
                    )
                )

            self.assertEqual(
                recipients.failed_tests_maintainers(checkout),
                expected,
            )

            models.Issue.objects.all().delete()

    def test_failed_test_maintainers_none(self):
        """Test failed_tests_maintainers function."""
        checkout = models.KCIDBCheckout.objects.get()
        tests = checkout.kcidbbuild_set.get().kcidbtest_set.all()
        tests.update(status='F', waived=False)

        self.assertEqual(
            recipients.failed_tests_maintainers(checkout),
            ['maint1@email.com', 'maint2@email.com']
        )

    def test_submitter(self):
        """Test submitter function."""
        checkout = models.KCIDBCheckout.objects.get()
        self.assertEqual(recipients.submitter(checkout), [])

        checkout.submitter = models.Maintainer.create_from_address('foo@bar.com')
        self.assertEqual(recipients.submitter(checkout), ['foo@bar.com'])


class TestRecipients(test.TestCase):
    """Tests for recipient parsing."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/checkouts_and_recipients.yaml',
    ]

    def test_recipients_keywords(self):
        """Test recipients_keywords content."""
        self.assertEqual(
            recipients.Recipients.recipients_keywords,
            {
                'failed_tests_maintainers': recipients.failed_tests_maintainers,
                'submitter': recipients.submitter,
            }
        )

    def test_evaluation_rules(self):
        """Test evaluation_rules content."""
        self.assertEqual(
            recipients.Recipients.evaluation_rules,
            {
                'always': recipients.when_always,
                'failed': recipients.when_failed,
                'failed_tests': recipients.when_failed_tests,
            }
        )

    def test_parse_recipients(self):
        """Test _parse_recipients function."""
        cases = [
            ('someone@mail.com', ['someone@mail.com']),
            (['someone@mail.com'], ['someone@mail.com']),
            ('failed_tests_maintainers', ['1@m.com', '2@m.com']),
            (['someone@mail.com', 'failed_tests_maintainers'],
             ['someone@mail.com', '1@m.com', '2@m.com']),
        ]

        recipients_keywords = {'failed_tests_maintainers': mock.Mock(return_value=['1@m.com', '2@m.com'])}
        with mock.patch('datawarehouse.recipients.Recipients.recipients_keywords', recipients_keywords):
            recipient = recipients.Recipients(mock.Mock())
            for recipients_input, expected_output in cases:
                self.assertEqual(
                    set(recipient._parse_recipients(recipients_input)),  # pylint: disable=protected-access
                    set(expected_output)
                )

    def test_conditions_match(self):
        """Test _conditions_match function."""
        # pylint: disable=protected-access
        evaluation_rules = {'foobar': mock.Mock(return_value=True)}
        with mock.patch('datawarehouse.recipients.Recipients.evaluation_rules', evaluation_rules):
            recipient = recipients.Recipients(mock.Mock())
            self.assertTrue(recipient._conditions_match({'when': 'foobar'}))
            self.assertTrue(evaluation_rules['foobar'].called)

            self.assertRaises(KeyError, recipient._conditions_match, {'when': 'missing-rule'})

    def test_render(self):
        """Test render returns the correct values."""
        rules = [
            {'when': 'always', 'send_to': ['a@a.com']},
            {'when': 'always', 'send_to': ['b@b.com'], 'report_template': 'long'},
            {'when': 'always', 'send_to': ['c@c.com', 'd@d.com'], 'send_bcc': ['e@e.com']},
            {'when': 'always', 'send_bcc': 'f@f.com'},
            {'when': 'always', 'send_to': 'failed_tests_maintainers', 'report_template': 'short'},
            {'when': 'always', 'send_to': ['c@c.com'], 'report_template': 'long'},
        ]
        models.KCIDBTest.objects.update(status='F', waived=False)
        checkout = models.KCIDBCheckout.objects.get()
        checkout.report_rules = rules

        recipient = recipients.Recipients(checkout)

        self.assertEqual(
            recipient.render(),
            {
                'default': {'To': {'a@a.com', 'd@d.com'}, 'BCC': {'e@e.com', 'f@f.com'}},
                'short': {'To': {'maint1@email.com', 'maint2@email.com'}},
                'long': {'To': {'b@b.com', 'c@c.com'}},
            }
        )

    def test_render_conditions_fail(self):
        """Test render returns the correct values when some of the "when" conditions fail."""
        rules = [
            {'when': 'always', 'send_to': ['a@a.com']},
            {'when': 'always', 'send_to': ['b@b.com'], 'report_template': 'short'},
            {'when': 'failed', 'send_to': ['b@b.com', 'c@c.com'], 'report_template': 'long'},
        ]
        checkout = models.KCIDBCheckout.objects.aggregated().get()
        checkout.report_rules = rules

        recipient = recipients.Recipients(checkout)

        self.assertEqual(
            recipient.render(),
            {
                'default': {'To': {'a@a.com'}},
                'short': {'To': {'b@b.com'}},
            }
        )
