"""RabbitMQ messaging implementation."""
import json

from cki_lib import messagequeue
from cki_lib.logger import get_logger
from django.db import transaction
import pika
import prometheus_client

from datawarehouse import models

LOGGER = get_logger(__name__)

MESSAGES_SENT = prometheus_client.Counter(
    'cki_dw_rabbitmq_messages_sent',
    'Count of sent messages',
    ['status']
)


class MessageQueue:
    """Messages Queue."""

    def __init__(self, host, port, user, password, keepalive_s=0):
        # pylint: disable=too-many-arguments
        """Init."""
        self.queue = messagequeue.MessageQueue(host=host, port=port, user=user,
                                               password=password, keepalive_s=keepalive_s)

    def send(self):
        """Send messages in the queue."""
        messages = (
            models.MessagePending.objects
            .select_for_update(skip_locked=True)
            .filter(kind=models.MessageKindEnum.RABBITMQ)
        )
        with transaction.atomic():
            for message in messages:
                data = json.loads(message.content['body'])
                headers = {'message-type': 'datawarehouse'}
                try:
                    self.queue.send_message(data=data, queue_name='', exchange=message.content['exchange'],
                                            headers=headers)
                    MESSAGES_SENT.labels('success').inc()
                except pika.exceptions.AMQPError:
                    LOGGER.exception('Error sending message, will be retried')
                    MESSAGES_SENT.labels('error').inc()
                    break
                else:
                    message.delete()

    def add(self, data, exchange):
        """Add message to the outgoing queue."""
        self.bulk_add([(data, exchange)])

    @staticmethod
    def bulk_add(messages_list):
        """
        Bulk add a list of messages to the outgoing queue.

        Parameters:
        messages_list:  a list of messages with the format (data, exchange).
        """
        msgs = [
            models.MessagePending(
                kind=models.MessageKindEnum.RABBITMQ,
                content={
                    'body': json.dumps(data),
                    'exchange': exchange
                }
            ) for data, exchange in messages_list
        ]
        models.MessagePending.objects.bulk_create(msgs)
