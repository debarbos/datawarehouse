# SPDX-License-Identifier: GPL-2.0-or-later
# Copyright (c) 2018-2019 Red Hat, Inc.

# pylint: disable=too-few-public-methods

"""Models related to messages."""

from django.db import models
from django_prometheus.models import ExportModelOperationsMixin as EMOM


class MessageKindEnum(models.IntegerChoices):
    """Kind of message."""

    RABBITMQ = 0
    EMAIL = 1


class MessagePending(EMOM('message_pending'), models.Model):
    """Model for MessagePending."""

    kind = models.IntegerField(choices=MessageKindEnum.choices)
    content = models.JSONField()

    def __str__(self):
        """Return __str__ formatted."""
        return str(self.id)

    class Meta:
        """Meta."""

        ordering = ('id', )
