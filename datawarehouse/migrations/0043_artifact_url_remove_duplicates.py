"""Populate patchset_hash."""
from django.db import migrations, models
from django.db.models import Count


def clean_artifacts(apps, schema_editor):
    """Split patchset_hash when applicable."""
    Artifact = apps.get_model('datawarehouse', 'Artifact')
    KCIDBBuild = apps.get_model('datawarehouse', 'KCIDBBuild')
    KCIDBCheckout = apps.get_model('datawarehouse', 'KCIDBCheckout')
    db_alias = schema_editor.connection.alias

    repeated = (
        Artifact.objects.using(db_alias)
        .values('url')
        .annotate(Count('id'))
        .order_by('-id__count')
        .filter(id__count__gt=1)
    )

    for repeated_log in repeated:
        all_repeated = (
            Artifact.objects.using(db_alias)
            .filter(url=repeated_log['url'])
            .order_by('id')
            .iterator()
        )
        to_keep = None

        for artifact in all_repeated:

            # Keep the first one
            if not to_keep:
                to_keep = artifact
                continue

            # KCIDBCheckout methods
            try:
                checkout = artifact.kcidbcheckout_set.get()
                checkout.log = to_keep
                checkout.save()
            except KCIDBCheckout.DoesNotExist:
                pass

            # KCIDBBuild methods
            try:
                build = artifact.kcidbbuild_set.get()
                build.log = to_keep
                build.save()
            except KCIDBBuild.DoesNotExist:
                pass

            for build in artifact.build_input.all():
                build.input_files.remove(artifact)
                build.input_files.add(to_keep)

            for build in artifact.build_output.all():
                build.output_files.remove(artifact)
                build.output_files.add(to_keep)

            # KCIDBTest methods
            for test in artifact.test.all():
                test.output_files.remove(artifact)
                test.output_files.add(to_keep)

            artifact.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0042_populate_patchset_hash'),
    ]

    operations = [
        migrations.RunPython(clean_artifacts),
    ]
